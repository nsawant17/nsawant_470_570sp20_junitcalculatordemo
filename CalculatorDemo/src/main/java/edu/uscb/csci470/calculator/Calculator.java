package edu.uscb.csci470.calculator;



/**
 * Calculator class that implements basic arithmetic operations as named methods
 * 
 * @author your_username@email.uscb.edu
 * @version 0.1
 * 
 *
 */


public class Calculator {

	public int add(int a, int b) {
		return a + b;
	} // end ,method add
	
	/**
	 * 
	 * @param a
	 * @param b
	 * @return
	 */

	public int subtract(int a, int b) {
		return a - b;
	} // end ,method subtract
/**
 * long is use for multiplication of two huge numbers
 * @param a
 * @param b
 * @return
 */
	public long multiply(int a, int b) {
		return a * b;
	} // end ,method multiply
/**
 * 
 * @param a
 * @param b
 * @return
 */
	public int intDivide(int a, int b) {
		int result;
		if (b == 0) {
			throw new IllegalArgumentException("intDivide method: cannot divide by zero");
		} else {
			result = a / b;
		}
		return result;

	} // end ,method divide
	/**
	 * Return the floating point division result of
	 * dividing a by b
	 * @param a
	 * @param b
	 * @return
	 */
	
	public double divide(int a, int b ) {
		double result;
		if (b == 0) {
			throw new IllegalArgumentException("intDivide method: cannot divide by zero");
		} else {
			result =(double) a /(double) b;
		}
		return result;
	}// end method divide

} // end class Calculator